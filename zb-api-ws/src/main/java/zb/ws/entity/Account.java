package zb.ws.entity;

import java.util.List;

import com.google.gson.Gson;

import lombok.Data;

@Data
public class Account {
	private String message;
	private String no;
	private String data;
	private long code;
	private String channel;
	private boolean success;

	public Result getData() {
		data = data.replaceAll(":", ":'").replaceAll(",", "',").replaceAll("\"", "").replaceAll("}'", "}").replaceAll("}", "'}").replace("coins:'", "coins:").replace("}'}", "}}");
		String removeStr = data.substring(data.indexOf("]',base:'"), data.lastIndexOf("}}"));
		data = data.replace(removeStr, "]").replace("}]}}", "}]}");
		return new Gson().fromJson(data, Result.class);
	}

	@Data
	public static class Result {
		private List<Coin> coins;
		private Base base;

		@Data
		public static class Coin {
			// private String cnName;//币种,跟key一样
			// private String enName;//币种,跟key一样
			private double available;
			private double freez;
			// private String unitTag;//币种,跟key一样
			/** 小数位 */
			private int unitDecimal;
			private String key;
		}

		/** 获取对应的coin,参数:ltc qc usdt... */
		public Coin getCoin(String coinName) {
			Coin coin = null;
			for (Coin c : coins) {
				if (c.getKey().equals(coinName)) {
					coin = c;
				}
			}
			return coin;
		}

		@Data
		public static class Base {
			private boolean auth_google_enabled;
			private boolean auth_mobile_enabled;
			private boolean trade_password_enabled;
			private String username;
		}
	}

}